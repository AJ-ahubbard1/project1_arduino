/**
 * \file project1_cli.ino 
 * ##A Command Line Interpreter for the Arduino Uno
 * ##Written by AJ HUBBARD
 * 
 */

#include "Arduino.h"
#include "HardwareSerial.h"
#include "TokenList.h"
#include "MyLights.h"
/**
 * RR Scheduler
 * timers  
*/
unsigned long timer = 0;
unsigned long uiTimer;
short UIWAIT=30;
unsigned long blinkTimer;
short BLINKWAIT = 3000; 
unsigned long parseTimer;
unsigned long switchTreeTimer;
const unsigned long MAX_WAIT = 0xFFFFFFFF;

/**
 * Get User Input 
*/
#define ENTER 13
#define SPACEBAR 32
#define BACKSPACE 127
#define DEBUG 47
#define LINE_BUF_SIZE 32
#define MAX_TOKEN_LENGTH 3
#define MAX_NUM_TOKENS 3
char userInput[LINE_BUF_SIZE];
char userTokens[MAX_NUM_TOKENS][MAX_TOKEN_LENGTH+1];
int incomingByte, tokenNum=0, tokenLength=0, bufSize=0;
int debug = 0;
int showStatus = 0;
int getBlink = 0;
char tokenBuffer[3];

char clear[5] = {0x1B,0x5B,0x32,0x4A,'\0'}; /** Esc[2J */

/**
 * LED PINS
*/
int leds[3][3] = {0};
#define stat_D13 0
#define stat_GREEN 1
#define stat_RED 2
int lastColor = RED;

void setup() {
	Serial.begin(9600);
	leds[stat_D13][PIN] = D13;
	leds[stat_GREEN][PIN] = GREEN;
	leds[stat_RED][PIN] = RED;
	for (int i = 0; i < NUM_LEDS; i++)
		pinMode(leds[i][PIN], OUTPUT);
	timer = millis();
	uiTimer = timer + UIWAIT;
	blinkTimer = timer + BLINKWAIT;
	parseTimer = MAX_WAIT;
	switchTreeTimer = MAX_WAIT;
	printVersion();
}


void loop() {
	timer = millis();
	if(timer > uiTimer) {
		uiTimer = timer + UIWAIT;
		getUserInput();
	}
	if(timer > blinkTimer) {
		blinkTimer = timer + BLINKWAIT/2;
		if(debug) {
			Serial.print("timer = ");
			Serial.print(timer);
			Serial.print("\tblink = ");
			Serial.println(blinkTimer);
		}
		if(leds[stat_D13][BLINK]) { 
			leds[stat_D13][ONOFF] = blinkLights(leds[stat_D13][ONOFF],leds[stat_D13][PIN]); 
		}
		// RED and GREEN both have blink set: swap colors
		if(leds[stat_RED][BLINK] && leds[stat_GREEN][BLINK]) {
			if(lastColor == RED) {
				biLedOff();
				leds[stat_GREEN][ONOFF] = lightsOn(GREEN);
				lastColor = GREEN;
			} else {
				biLedOff();
				leds[stat_RED][ONOFF] = lightsOn(RED);
				lastColor = RED;
			}
			leds[stat_RED][BLINK] = 1;		
			leds[stat_GREEN][BLINK] = 1;
		} else { 
			if(leds[stat_GREEN][BLINK]) { 
				leds[stat_GREEN][ONOFF] = blinkLights(leds[stat_GREEN][ONOFF],leds[stat_GREEN][PIN]); 
			}
			if(leds[stat_RED][BLINK]) { 
				leds[stat_RED][ONOFF] = blinkLights(leds[stat_RED][ONOFF],leds[stat_RED][PIN]); 
			}
		}
		if(showStatus)
			showStatusLeds();
	}
	if(timer > parseTimer) {
		parseTimer = MAX_WAIT;
		parseInput(bufSize);
	}
	if(timer > switchTreeTimer) {
		switchTreeTimer = MAX_WAIT;
		switchTree();
	}
}

void getUserInput() {
	if(Serial.available() > 0) {
		incomingByte = Serial.read();

		//convert lowercase to uppercase
		if(incomingByte >= 'a' && incomingByte <= 'z')
			incomingByte-= 'a' - 'A';
		
		//save letters,numbers, and spaces in uibuffer
		if((incomingByte >= 'A' && incomingByte <= 'Z') ||
		   (incomingByte >= '0' && incomingByte <= '9') ||
			 (incomingByte == SPACEBAR)) { 
			userInput[bufSize++] = incomingByte;
		} 
		//handle backspace
		if(incomingByte == BACKSPACE && bufSize >= 1) {
				userInput[--bufSize] = '\0';
				refreshScreen();
				//Serial.println();
				//Serial.print(userInput);
		}
		if(incomingByte == DEBUG)
			debug = !debug;
		
		refreshScreen();
		
		//handle enter or buffer full
		if(incomingByte == ENTER || bufSize >= LINE_BUF_SIZE-1) {
			if (bufSize >= LINE_BUF_SIZE-1) 
				userInput[LINE_BUF_SIZE-1] = '\0';
			else 
				userInput[bufSize] = '\0';
			Serial.println();
			Serial.println(userInput);
			if(getBlink) {
				int value = atoi(userInput);
				getBlink = 0;
				if (value >= 100) {
					BLINKWAIT = value;
					Serial.print("blink set to: ");
					Serial.println(BLINKWAIT);
				} else {
					Serial.println("Value must be > 100");
					Serial.print("blink set to: ");
					Serial.println(BLINKWAIT);
				}
			}
			
			parseTimer = timer + UIWAIT;
		}
	}
}

void parseInput(int _bufSize) {
	int i = 0;
	clearTokens();
	while(i < _bufSize && tokenNum < MAX_NUM_TOKENS) {
		if(userInput[i] != SPACEBAR) {
			if(tokenLength < MAX_TOKEN_LENGTH) 
				userTokens[tokenNum][tokenLength] = userInput[i];
			tokenLength++;
		}
		if(userInput[i] == SPACEBAR || i == _bufSize-1) {
			if(tokenLength > 0) {
				userTokens[tokenNum][MAX_TOKEN_LENGTH] = '\0';
				userTokens[tokenNum][2] = (short)tokenLength;
			
				// Need to lookup token in table
				short tokenId = getTokenNumber(userTokens[tokenNum], debug);
				userTokens[tokenNum][3] = tokenId;
				tokenNum++;
				tokenLength = 0;
			}
		}
		i++;
	}
	if(debug)
		showUserTokens();
	
	switchTreeTimer = timer + UIWAIT;
	bufSize = 0;
}
void clearTokens() {
	for(int n = 0; n < MAX_NUM_TOKENS; n++) 
		for(int l = 0; l < MAX_TOKEN_LENGTH; l++)
			userTokens[n][l] = '\0';
	tokenNum = tokenLength = 0;
	 
}
void refreshScreen() {
	Serial.println(clear);
	for(int i = 0; i < bufSize; i++)
		Serial.print(userInput[i]);
}
void showUserTokens() {
	int length = 0;
	int tokId = 0;
	for(int i = 0; i < tokenNum; i++) {
		length = userTokens[i][2];
		tokId = userTokens[i][3];
		Serial.print(i);
		Serial.print(": '");
		Serial.print(userTokens[i][0]);
		Serial.print("', ");
		
		Serial.print("'");
		Serial.print(userTokens[i][1]);
		Serial.print("', ");
		
		Serial.print("'");
		Serial.print(length);
		Serial.print("', ");
		
		Serial.print("'");
		Serial.print(tokId);
		Serial.println("'");
	}
}
void switchTree() {
	switch(userTokens[0][3]) {

		/** Built-in Light Functions */
		case t_D13:
			switch(userTokens[1][3]) {
				case t_ON:  													//
					leds[stat_D13][ONOFF] = lightsOn(D13);
					break;
				case t_OFF:														//
					leds[stat_D13][ONOFF] = lightsOff(D13);
					leds[stat_D13][BLINK] = 0;
					break;
				case t_BLINK:
					leds[stat_D13][ONOFF] = lightsOn(D13);
					leds[stat_D13][BLINK] = 1;
					break;
				default: break;
			}
			break;
		
		/** Bi-Color LED Functions */
		case t_LED:
			switch(userTokens[1][3]) {
				case t_ON:
					if(lastColor == RED) {
						leds[stat_RED][ONOFF] = lightsOn(RED);
						leds[stat_RED][BLINK] = 0;
					}
					else { 
						leds[stat_GREEN][ONOFF] = lightsOn(GREEN);
						leds[stat_GREEN][BLINK] = 0;
					}
					break;
				case t_GREEN:													//
					biLedOff();
					leds[stat_GREEN][ONOFF] = lightsOn(GREEN);
					lastColor = GREEN;
					break;
				case t_RED:														//
					biLedOff();
					leds[stat_RED][ONOFF] = lightsOn(RED);
					lastColor = RED;
					break;
				case t_OFF:														//
					biLedOff();
					break;
				case t_BLINK:
					if(lastColor == RED) {
						leds[stat_RED][ONOFF] = lightsOn(RED);
						leds[stat_RED][BLINK] = 1;
					} else {
						leds[stat_GREEN][ONOFF] = lightsOn(GREEN);
						leds[stat_GREEN][BLINK] = 1;
					}
					break;
				default: break;
			}
			break;

		// RED GREEN FUNCTION
		case t_RED:
			switch(userTokens[1][3]) {
				case t_GREEN:
						if(lastColor == RED) {
							biLedOff();
							leds[stat_RED][ONOFF] = lightsOn(RED);
						} else {
							biLedOff();
							leds[stat_GREEN][ONOFF] = lightsOn(GREEN);
						}
						leds[stat_RED][BLINK] = 1;		
						leds[stat_GREEN][BLINK] = 1;
					break;
				default: break;
			}
		/** Change Blink Rate */
		case t_SET:
			switch(userTokens[1][3]) {
				case t_BLINK:
					setBlink();
					break;
				default:
					break;
			}	
			break;
		case t_STATUS:
			switch(userTokens[1][3]) {
				case t_LEDS:
					showStatus = !showStatus;
					break;
				default: break;
			}	
			break;
		case t_VERSION:
			printVersion();
			break;
		case t_HELP:
			printHelp();
			break;
		default:
			break;
	}
}
void biLedOff() {
	leds[stat_RED][ONOFF] = lightsOff(RED);
	leds[stat_GREEN][ONOFF] = lightsOff(GREEN);
	leds[stat_RED][BLINK] = 0;
	leds[stat_GREEN][BLINK] = 0;
}
void showStatusLeds() {
	//Serial.println(clear);
	Serial.println("LED\tON/OFF\tBLINK");
	for(int i = 0; i < NUM_LEDS; i++) {
		for(int j = 0; j < 3; j++) {
			Serial.print(leds[i][j]);
			Serial.print("\t");
		}
		Serial.println();
	}
}
void setBlink() {
	Serial.println("Input blink time in ms, should be >= 100");
	Serial.print("current time: ");
	Serial.println(BLINKWAIT);
	getBlink = 1;
	
}
void printVersion() {
	Serial.println("Arduino CLI Version 1.7");
}
void printHelp() {
  Serial.println("Available Commands");
  Serial.println("D13 ON|OFF|BLINK");
  Serial.println("LED GREEN|RED|ON|OFF|BLINK");
  Serial.println("RED GREEN");
  Serial.println("SET BLINK");
  Serial.println("STATUS LEDS");
  Serial.println("VERSION");
  Serial.println("HELP");
}
