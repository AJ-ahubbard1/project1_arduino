PERM=`stat -c "%a" /dev/ttyACM0`
if (( $PERM < 777 )); then
	echo "USB is $PERM"
	sudo chmod 777 /dev/ttyACM0
fi
PERM=`stat -c "%a" /dev/ttyACM0`
echo "now USB is $PERM"
picocom -b 9600 /dev/ttyACM0
