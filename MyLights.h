#ifndef MyLights_h
#define MyLights_h

#define NUM_LEDS 3
#define RED 2
#define GREEN 3
#define D13 13
#define PIN 0
#define ONOFF 1
#define BLINK 2

int lightsOn(int pin) {
	digitalWrite(pin, HIGH);
	return 1;
}
int lightsOff(int pin) {
	digitalWrite(pin, LOW);
	return 0;
}
int blinkLights(int isOn, int pin) {
	if (isOn) {
		lightsOff(pin);
		return 0;
	} else {
		lightsOn(pin);
		return 1;
	}
}

#endif
