#ifndef TokenList_h
#define TokenList_h


#define t_D13 0
#define t_LED 1
#define t_SET 2
#define t_STATUS 3
#define t_VERSION 4
#define t_HELP 5
#define t_ON 6
#define t_OFF 7
#define t_BLINK 8
#define t_GREEN 9
#define t_RED 10
#define t_LEDS 11
#define t_EOL 255
#define NUM_TOKENS 13

char tokenTable[] = {
	'D','1',0x03, t_D13,
	'L','E',0x03, t_LED,
	'S','E',0x03, t_SET,
	'S','T',0x06, t_STATUS,
	'V','E',0x07, t_VERSION,
	'H','E',0x04, t_HELP,
	'O','N',0x02, t_ON,
	'O','F',0x03, t_OFF,
	'B','L',0x05, t_BLINK,
	'G','R',0x05, t_GREEN,
	'R','E',0x03, t_RED,
	'L','E',0x04, t_LEDS,
	'E','O',0x03, t_EOL
};

short getTokenNumber(char *uToken, int _debug) {
	int i = 0;
	int match = 0;
 	int result = t_EOL;
	while(i < 4*NUM_TOKENS) {
		if (_debug) {
			Serial.print("i = ");
			Serial.print(i);
			Serial.print(" match = ");
			Serial.print(match);
			Serial.print("\tComparing User: ");
			Serial.print(uToken[match]);
			Serial.print("  Table: ");
			Serial.println(tokenTable[i+match]);
		}
		if(tokenTable[i+match] == uToken[match])
			match++;
		else {
			match = 0;
			i+=4;
		}

		if(match == 3) {  
			result = tokenTable[i+3];
			break;
		}
	} 
	return result;
}

#endif
